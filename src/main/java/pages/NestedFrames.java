package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class NestedFrames {

    private WebDriver driver;
    private String frameLeft = "frame-left";
    private String frameTop = "frame-top";
    private String frameBottom = "frame-bottom";
    private By body = By.tagName("body");

    public NestedFrames(WebDriver driver) {
        this.driver = driver;
    }

    public String getTextFromLeftEditor(){
        switchToTopFrame();
        switchToLeftFrame();
        String text = driver.findElement(body).getText();
        switchFrameToParent(); //switch to left
        switchFrameToParent(); //switch to main
        return text;
    }

    public String getTextFromBottomEditor(){
        switchToBottomFrame();
        String text = driver.findElement(body).getText();
        switchFrameToParent();
        return text;
    }

    private void switchToLeftFrame(){
        driver.switchTo().frame(frameLeft);
    }

    private void switchToBottomFrame(){
        driver.switchTo().frame(frameBottom);
    }

    private void switchToTopFrame(){
        driver.switchTo().frame(frameTop);
    }

    private void switchFrameToParent() {
        driver.switchTo().parentFrame();
    }

}
