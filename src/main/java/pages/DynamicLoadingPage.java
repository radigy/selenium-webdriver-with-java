package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import javax.naming.ldap.Control;

public class DynamicLoadingPage {

    private WebDriver driver;
    private By Example1 = By.xpath(".//a[contains(text(), 'Example 1')]");
    private By Example2 = By.xpath(".//a[contains(text(), 'Example 2')]");

    public DynamicLoadingPage(WebDriver driver) {
        this.driver = driver;
    }

    public DynamicLoadingExample1Page clickExample1(){
        driver.findElement(Example1).click();
        return new DynamicLoadingExample1Page(driver);
    }

    public DynamicLoadingExample2Page clickExample2(){
        driver.findElement(Example2).click();
        return new DynamicLoadingExample2Page(driver);
    }

    public DynamicLoadingExample2Page openInNewTabExample2(){
        driver.findElement(Example2).sendKeys(Keys.CONTROL, Keys.ENTER);
        return new DynamicLoadingExample2Page(driver);
    }



}
