package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class ContextMenuPage {

    private WebDriver driver;
    private By triangle = By.id("hot-spot");

    public ContextMenuPage(WebDriver driver) {
        this.driver = driver;
    }

    public void rightClickTriangle() {
        Actions actions = new Actions(driver);
        actions.contextClick(driver.findElement(triangle)).perform();
    }

    public String getTextFromContextMenuAlert(){
        return driver.switchTo().alert().getText();
    }

    public void acceptContextMenuPopUp(){
        driver.switchTo().alert().accept();
    }


}
