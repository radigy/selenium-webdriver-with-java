package alerts;

import base.BaseTest;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class AlertsTests extends BaseTest {

    @Test
    public void testAcceptAlert() {
        var alertsPage = homePage.clickAlertsPage();
        alertsPage.triggerAlert();
        alertsPage.alert_clickToAccept();
        alertsPage.getAlertResult();
        assertEquals(alertsPage.getAlertResult(), "You successfuly clicked an alert", "Result test incorrect");
    }

    @Test
    public void testGetTextFromAlert() {
        var alertsPage = homePage.clickAlertsPage();
        alertsPage.triggerConfirm();
        String text = alertsPage.alert_getText();
        alertsPage.alert_clickToDismiss();
        assertEquals(text, "I am a JS Confirm", "Alert text incorrect");

    }

    @Test
    public void testSetInputInAlert(){
        var alertsPage = homePage.clickAlertsPage();
        alertsPage.triggerPrompt();
        String text = "TAU rocks!";
        alertsPage.alert_setInput(text);
        alertsPage.alert_clickToAccept();
        assertEquals(alertsPage.getAlertResult(), "You entered: " + text, "Result text incorrect");
    }

}
