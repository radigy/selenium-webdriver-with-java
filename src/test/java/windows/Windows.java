package windows;

import base.BaseTest;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class Windows extends BaseTest {

    @Test
    public void testWindowsTabs(){
        var buttonPage = homePage.clickDynamicLoading().openInNewTabExample2();
        getWindowManager().switchToNewTab();
        assertTrue(buttonPage.isStartButtonDisplayed(), "start button is not displayed");
    }

}
