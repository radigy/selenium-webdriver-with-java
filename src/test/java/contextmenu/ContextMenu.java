package contextmenu;

import base.BaseTest;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertEquals;

public class ContextMenu extends BaseTest {

    @Test
    public void testContextMenuAlert() {
        var contextMenu = homePage.clickContextMenu();
        contextMenu.rightClickTriangle();
        String message = contextMenu.getTextFromContextMenuAlert();
        contextMenu.acceptContextMenuPopUp();
        assertEquals(message, "You selected a context menu", "Text on Context Menu Modal is not visible");
    }


}
