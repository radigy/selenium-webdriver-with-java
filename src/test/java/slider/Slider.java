package slider;

import base.BaseTest;
import org.testng.annotations.Test;
import pages.LoginPage;
import pages.SliderPage;

import static org.testng.Assert.assertEquals;

public class Slider extends BaseTest {

    @Test
    public void testSlider(){
        String value = "5";
        SliderPage sliderPage = homePage.clickHorizontalSlider();
        sliderPage.clickOnSlider();
        sliderPage.setSliderValue(value);
        assertEquals(sliderPage.getSliderValue(), value, "Slider value is incorrect");

    }

}
