package frames;

import base.BaseTest;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class NestedFrames extends BaseTest {

    @Test
    public void testNestedFrames(){
        homePage.clickFrames();
        var nested = homePage.clickNestedFrames();
        String left = nested.getTextFromLeftEditor();
        String bottom = nested.getTextFromBottomEditor();
        assertEquals(left, "LEFT", "Left frame text is not correct");
        assertEquals(bottom, "BOTTOM", "Bottom frame is not correct");
    }
}
