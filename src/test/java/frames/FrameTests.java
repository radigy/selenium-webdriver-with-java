package frames;

import base.BaseTest;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertEquals;

public class FrameTests extends BaseTest {

    @Test
    public void testWysiwyg(){
        var editorPage = homePage.clickWysiwygEditor();
        editorPage.clearInput();
        String text1 = "Hello";
        String text2 = "World";
        editorPage.setInput(text1);
        editorPage.clickDecreaseIndentionButton();
        editorPage.setInput(text2);
        assertEquals(editorPage.getTextFromEditor(), text1+text2, "text from editor is incorrect");

    }

    public void pause(Integer milliseconds){
        try {
            TimeUnit.MILLISECONDS.sleep(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
