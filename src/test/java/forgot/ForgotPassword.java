package forgot;

import base.BaseTest;
import org.testng.annotations.Test;
import pages.ForgotPasswordPage;

import static org.testng.Assert.assertTrue;


public class ForgotPassword extends BaseTest {

    @Test
    public void testForgotPassword() {

        ForgotPasswordPage forgotPassword = homePage.clickForgotPassword();

        forgotPassword.setEmail("tau@example.com");
        var emailSentPage = forgotPassword.clickRetrivePassword();
        emailSentPage.getAlertText();
        assertTrue(emailSentPage.getAlertText().contains("Your e-mail's been sent"), ("Alert text is incorrect"));

    }
}
